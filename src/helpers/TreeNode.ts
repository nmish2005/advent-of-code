const util = require('util')

export default class TreeNode {
    public value: any
    public children: Array<any>

    public constructor (value: any) {
        this.value = value
        this.children = []
    }

    public push (...values: any): TreeNode {
        this.children.push(...values)

        return this
    }

    public get branchCount () {
        return this.children.reduce((acc, child) => {
            acc += 1 + child.branchCount

            return acc
        }, 0)
    }

    public visualize (): void {
        console.log(util.inspect(this, false, null, true))
    }
}
