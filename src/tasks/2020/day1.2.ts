import DataProvider from "../../helpers/DataProvider";
import { sessionId, year } from "../../env";

async function run () {
    const provider = new DataProvider({
        taskId: 1,
        year,
        sessionId
    })
    let intArray = (await provider.getData())
        .split('\n')
        .map(Number)

    let multipliedEntries = 0

    for (let i = 0; i < intArray.length; i++) {
        for (let j = i + 1; j < intArray.length; j++) {
            for (let k = j + 1; k < intArray.length; k++) {
                if (intArray[i] + intArray[j] + intArray[k] === 2020) {
                    multipliedEntries = intArray[i] * intArray[j] * intArray[k]
                }
            }
        }
    }

    return multipliedEntries
}

run()
    .then(console.log)
