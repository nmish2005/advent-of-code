import DataProvider from "../../helpers/DataProvider";
import {sessionId, year} from "../../env";

class PassportValidator {
    entries: Array<Array<string>>
    validCount: number

    constructor(rawData: string) {
        this.validCount = 0
        this.entries = PassportValidator.prepareData(rawData)

        this.run()
    }

    public static validateEntry(entry: string): boolean {
        const [key, value] = entry.split(':')
        let flag = false

        switch (key) {
            case 'byr':
                if (Number(value) >= 1920 &&
                    Number(value) <= 2002) {
                    flag = true
                }

                break

            case 'iyr':
                if (Number(value) >= 2010 &&
                    Number(value) <= 2020) {
                    flag = true
                }

                break

            case 'eyr':
                if (Number(value) >= 2020 &&
                    Number(value) <= 2030) {
                    flag = true
                }

                break

            case 'hgt':
                if (value.includes('cm')) {
                    if (parseInt(value) >= 150 &&
                        parseInt(value) <= 193) {
                        flag = true
                    }
                } else if (value.includes('in')) {
                    if (parseInt(value) >= 59 &&
                        parseInt(value) <= 76) {
                        flag = true
                    }
                }

                break

            case 'hcl':
                if (/^#[0-9A-F]{6}$/i.test(value)) {
                    flag = true
                }

                break

            case 'ecl':
                if (['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(value)) {
                    flag = true
                }

                break

            case 'pid':
                if (value.length === 9 &&
                    value.split('').every(digit => !isNaN(Number(digit)))) {
                    flag = true
                }

                break

            case 'cid':
                flag = true

                break

            default:
                throw new Error('Entry not recognized')
        }

        return flag
    }

    private run(): void {
        this.entries.forEach(entry => {
            const validationResults = entry.map(PassportValidator.validateEntry),
                rawData = entry.join(' ')

            if (rawData.includes('byr') &&
                rawData.includes('iyr') &&
                rawData.includes('eyr') &&
                rawData.includes('hgt') &&
                rawData.includes('hcl') &&
                rawData.includes('ecl') &&
                rawData.includes('pid') &&
                !validationResults.includes(false)) {
                this.validCount++
            }
        })
    }

    private static prepareData(rawData: string): Array<Array<string>> {
        return rawData
            .replace(/\n/g, ' ')
            .split('  ')
            .map(entry => entry.split(' ')
                .filter(entry => entry.length > 0))
    }

    public get score(): number {
        return this.validCount
    }
}

async function run() {
    const provider = new DataProvider({
        taskId: 4,
        year,
        sessionId
    })
    let rawData = (await provider.getData())

    const Validator = new PassportValidator(rawData)

    return Validator.score
}

run()
    .then(console.log)
