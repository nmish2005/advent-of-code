import '../../helpers/extensions/String.ext'
import DataProvider from "../../helpers/DataProvider"
import { sessionId, year } from "../../env"

class Validator {
    public static processBatch (rawData: Array<string>): number {
        let validPasswords = 0

        rawData.forEach(item => {
            if (Validator.runWorker(item)) {
                validPasswords++
            }
        })

        return validPasswords
    }

    private static runWorker (item: string): boolean {
        let [rule,  password] = item.split(': '),
            [range, symbol]   = rule.split(' '),
            indexes           = range.split('-').map(Number)

        return Number(password[indexes[0] - 1] === symbol)
             + Number(password[indexes[1] - 1] === symbol)
             === 1
    }
}

async function run () {
    const provider = new DataProvider({
        taskId: 2,
        year,
        sessionId
    }),
        rawData = (await provider.getData())
                         .split('\n')

    rawData.pop()

    return Validator.processBatch(rawData)
}

run()
    .then(console.log)
