import './extensions/Array.ext'

export default class GameConsole {
    private rules: Array<{ operation: string, value: number }>
    private accumulator: number

    public constructor(rawData: string) {
        this.rules = GameConsole.prepareData(rawData)
        this.accumulator = 0
    }

    public get accumulatedValue (): number {
        const processedIndexes = []
        let nextIndex = this.runCommand(0)

        while (!processedIndexes.includes(nextIndex)) {
            processedIndexes.push(nextIndex)
            nextIndex = this.runCommand(nextIndex)
        }

        return this.accumulator
    }

    private checkCodeValidity (rules: Array<{ operation: string, value: number }>): boolean {
        const processedIndexes = [],
            oldAccumulator = this.accumulator
        let nextIndex = this.runCommand(0, rules)

        while (true) {
            if (processedIndexes.includes(nextIndex)) {
                this.accumulator = oldAccumulator

                return false
            } else if (!rules[nextIndex]) {
                this.accumulator = oldAccumulator

                return true
            }

            processedIndexes.push(nextIndex)
            nextIndex = this.runCommand(nextIndex, rules)
        }
    }

    public fixProgram (): void {
        const nopOccurences = this.rules.map(i => i.operation)
            .getOccurenceIndexes('nop'),
            jmpOccurences = this.rules.map(i => i.operation)
                .getOccurenceIndexes('jmp')
        let rules = JSON.parse(JSON.stringify(this.rules))

        for (const key in nopOccurences) {
            if (!nopOccurences.hasOwnProperty(key)) {
                continue
            }

            rules[nopOccurences[key]].operation = 'jmp'

            if (this.checkCodeValidity(rules)) {
                this.rules = rules
                return
            } else {
                rules = JSON.parse(JSON.stringify(this.rules))
            }
        }

        for (const key in jmpOccurences) {
            if (isNaN(Number(key))) {
                continue
            }

            rules[jmpOccurences[key]].operation = 'nop'

            if (this.checkCodeValidity(rules)) {
                this.rules = rules
                return
            } else {
                rules = JSON.parse(JSON.stringify(this.rules))
            }
        }
    }

    private runCommand (currentIndex: number, rules: Array<{ operation: string, value: number }> = this.rules): number {
        const rule = rules[currentIndex]

        if (!rule) {
            return 0
        }

        switch (rule.operation) {
            case 'nop':
                return ++currentIndex

            case 'jmp':
                return currentIndex + rule.value

            case 'acc':
                this.accumulator += rule.value
                return ++currentIndex

            default:
                throw new Error('Invalid data.')
        }
    }

    private static prepareData (raw: string): Array<{ operation: string, value: number }> {
        const rules = raw
            .split('\n')
            .map(rule => ({
                operation: rule.split(' ')[0],
                value: parseInt(rule.split(' ')[1])
            }))

        rules.pop()

        return rules
    }
}
