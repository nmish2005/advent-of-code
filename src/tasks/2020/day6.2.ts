import DataProvider from "../../helpers/DataProvider";
import { sessionId, year } from "../../env";
import CDForm from "../../helpers/CDForm";

async function run () {
    const provider = new DataProvider({
        taskId: 6,
        year,
        sessionId
    })
    let rawData = (await provider.getData())

    const Form = new CDForm(rawData)

    Form.countCoincidentAnswers()

    return Form.realSum
}

run()
    .then(console.log)
