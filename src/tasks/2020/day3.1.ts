import DataProvider from "../../helpers/DataProvider";
import { sessionId, year } from "../../env";

interface Directions {
    left ?: number,
    right ?: number,
    down ?: number
}

class Bloodhound {
    private map: Array<Array<string>>
    private direction: Directions
    private position: { x: number, y: number }
    private treeCounter: number

    constructor(map: Array<Array<string>>, direction: Directions) {
        this.map = map

        if (!direction.down) {
            direction.down = 0
        }

        if (!direction.left) {
            direction.left = 0
        }

        if (!direction.right) {
            direction.right = 0
        }

        this.direction = direction
        this.treeCounter = 0
        this.position = { x: 0, y: 0 }
    }

    public run () {
        this.position = {
            x: this.position.x + this.direction.right - this.direction.left,
            y: this.position.y + this.direction.down
        }

        if (this.position.y >= this.map.length) {
            return
        } else if (this.position.x >= this.map[0].length) {
            this.position.x -= this.map[0].length
        }

        if (this.map[this.position.y][this.position.x] === '#') {
            this.treeCounter++
        }

        this.run()
    }

    public get treeCount () {
        return this.treeCounter
    }
}

async function run () {
    const provider = new DataProvider({
        taskId: 3,
        year,
        sessionId
    })
    let map = (await provider.getData())
        .split('\n')
        .map(row => row.split(''))

    const BH = new Bloodhound(map, { right: 3, down: 1 })

    BH.run()

    return BH.treeCount
}

run()
    .then(console.log)
