import DataProvider from "../../helpers/DataProvider";
import { sessionId, year } from "../../env";

class Airplane {
    private seats: Array<string>
    private lastRange: {
        row: {
            min: number,
            max: number
        },
        column: {
            min: number,
            max: number
        }
    }
    private seatIds: Array<number>

    public constructor (seats: Array<string>) {
        this.seats = seats
        this.seatIds = []
    }

    private flushRange (): void {
        this.lastRange = {
            row: {
                min: 0,
                max: 127
            },
            column: {
                min: 0,
                max: 7
            }
        }
    }

    public processBatch (): void {
        this.seats.forEach(seat => {
            this.flushRange()

            const { row, column } = this.parseSeat(seat.split('')),
                seatID = (row * 8) + column

            if (row !== 0 &&
                row !== 127) {
                this.seatIds.push(seatID)
            }
        })
    }

    private parseSeat (specs: Array<string>, i: number = 0)
        : { row: number, column: number } {
        if (i >= specs.length) {
            return {
                row: this.lastRange.row.min,
                column: this.lastRange.column.min
            }
        }

        switch (specs[i]) {
            case 'F':
                this.lastRange.row.max = Math.floor((this.lastRange.row.min + this.lastRange.row.max) / 2)
                break

            case 'B':
                this.lastRange.row.min = Math.ceil((this.lastRange.row.min + this.lastRange.row.max) / 2)
                break

            case 'L':
                this.lastRange.column.max = Math.floor((this.lastRange.column.min + this.lastRange.column.max) / 2)
                break

            case 'R':
                this.lastRange.column.min = Math.ceil((this.lastRange.column.min + this.lastRange.column.max) / 2)
                break

            default:
                throw new Error('Incorrect seat specification: ' + specs.join(''))
        }

        return this.parseSeat(specs, ++i)
    }

    public get missingId () {
        const arr = this.seatIds.sort((a, b) => a - b)

        return arr.find((item, i) => arr[i + 1] !== ++item) + 1
    }
}

async function run () {
    const provider = new DataProvider({
        taskId: 5,
        year,
        sessionId
    })
    let rawData = (await provider.getData())
        .split('\n')

    const SeatProcessor = new Airplane(rawData)

    SeatProcessor.processBatch()

    return SeatProcessor.missingId
}

run()
    .then(console.log)
