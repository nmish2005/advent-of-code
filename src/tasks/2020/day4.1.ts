import DataProvider from "../../helpers/DataProvider";
import { sessionId, year } from "../../env";

class PassportValidator {
    entries: Array<Array<string>>
    validCount: number

    constructor (rawData: string) {
        this.validCount = 0
        this.entries = PassportValidator.prepareData(rawData)

        this.run()
    }

    private run (): void {
        this.entries.forEach(entry => {
            if (entry.includes('byr') &&
                entry.includes('iyr') &&
                entry.includes('eyr') &&
                entry.includes('hgt') &&
                entry.includes('hcl') &&
                entry.includes('ecl') &&
                entry.includes('pid')) {
                this.validCount++
            }
        })
    }

    private static prepareData (rawData: string): Array<Array<string>> {
        return rawData
            .replace(/\n/g, ' ')
            .split('  ')
            .map(entry => entry.split(' ')
                .map(field => field.split(':')[0]))
    }

    public get score (): number {
        return this.validCount
    }
}

async function run () {
    const provider = new DataProvider({
        taskId: 4,
        year,
        sessionId
    })
    let rawData = (await provider.getData())

    const Validator = new PassportValidator(rawData)

    return Validator.score
}

run()
    .then(console.log)
