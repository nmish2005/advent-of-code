import './extensions/Array.ext'
import TreeNode from "./TreeNode";

export default class LuggageProcessor {
    private readonly rules: { [id: string]: Array<string> }
    private ignoredBags: Array<string>
    private tree: TreeNode

    public constructor(rawData: string, searchItem: string) {
        this.rules = LuggageProcessor.prepareData(rawData)
        this.ignoredBags = []

        this.tree = new TreeNode(searchItem)

        this.tree.push(...this.buildTree(searchItem)).visualize()
    }

    private buildTree(searchItem: string): any {
        const branches = []

        for (const rule in this.rules) {
            if (this.rules[rule].includes(searchItem) &&
                !this.ignoredBags.includes(rule)) {
                branches.push(new TreeNode(rule))
            }
        }

        this.ignoredBags.push(searchItem)

        branches.map(branch => {
            branch.push(...this.buildTree(branch.value))

            return branch
        })

        return branches
    }

    private static prepareData(raw: string): { [id: string]: Array<string> } {
        const parsedRules = {}

        raw.split('\n')
            .forEach(stringRule => {
                if (stringRule.length === 0) {
                    return
                }

                parsedRules[stringRule.split(' bags contain')[0]] =
                    stringRule.split(' bags contain')[1]
                        .replace('.', '')
                        .replace(/[0-9]/g, '')
                        .split(',')
                        .map(str => {
                            str = str
                                .replace(/bags|bag/g, '')
                                .trim()

                            if (str !== 'no other') {
                                return str
                            }
                        })
                        .filter(items => items !== undefined)
            })

        return parsedRules
    }

    public get countOfContainerColors() {
        return this.tree.branchCount
    }
}
