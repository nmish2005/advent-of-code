import DataProvider from "../../helpers/DataProvider";
import { sessionId, year } from "../../env";

async function run () {
    const provider = new DataProvider({
        taskId: 1,
        year,
        sessionId
    })
    let intArray = (await provider.getData())
                    .split('\n')
                    .map(Number)

    let multipliedEntries = 0

    for (let i = 0; i < intArray.length; i++) {
        for (let j = i + 1; j < intArray.length; j++) {
            if (intArray[i] + intArray[j] === 2020) {
                multipliedEntries = intArray[i] * intArray[j]
            }
        }
    }

    return multipliedEntries
}

run()
    .then(console.log)
