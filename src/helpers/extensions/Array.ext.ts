interface Array<T> {
    unique (): Array<T>
    deepFlat (): Array<T>
    getOccurenceIndexes (searchItem: any): Array<number>
}

Array.prototype.unique = function () {
    return [...new Set(this)]
}

Array.prototype.deepFlat = function () {
    if (this.some(Array.isArray)) {
        return this.flat().deepFlat()
    } else {
        return this
    }
}

Array.prototype.getOccurenceIndexes = function (searchItem) {
    return this.flatMap((item, i) => item === searchItem ? i : [])
}
