import DataProvider from "../../helpers/DataProvider";
import { sessionId, year } from "../../env";
import GameConsole from "../../helpers/GameConsole";

async function run () {
    const provider = new DataProvider({
        taskId: 8,
        year,
        sessionId
    })
    let rawData = (await provider.getData())
    const Console = new GameConsole(rawData)

    return Console.accumulatedValue
}

run()
    .then(console.log)
