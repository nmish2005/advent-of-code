import fetch from 'node-fetch'

export default class DataProvider {
    private readonly year: number
    private readonly taskId: number
    private readonly sessionId: string
    private url: string

    public constructor (settings: {year: number, taskId: number, sessionId: string}) {
        this.year = settings.year || (new Date()).getFullYear()
        this.taskId = settings.taskId || 1
        this.sessionId = settings.sessionId || ''

        this.buildURL()
    }

    private buildURL (): void {
        this.url = `https://adventofcode.com/${this.year}/day/${this.taskId}/input`
    }

    public async getData (): Promise<string> {
        try {
            const request = await fetch(this.url, {
                headers: {
                    cookie: `session=${this.sessionId}`
                }
            })

            return await request.text()
        } catch (e) {
            throw e
        }
    }
}
