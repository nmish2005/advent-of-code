interface String {
    countSymbolOccurences (symbol: string): number;
}

String.prototype.countSymbolOccurences = function (symbol) {
    return this.split(symbol).length - 1
}
