import './extensions/Array.ext'

export default class CDForm {
    groups: Array<{ answers: Array<string>, count: number, realCount: number }>

    constructor (rawData: string) {
        this.groups = CDForm.prepareData(rawData)
    }

    private static prepareData (rawData: string): Array<{ answers: Array<string>, count: number, realCount: number }> {
        return rawData
            .split('\n\n')
            .map(group => ({
                answers: group.split('\n'),
                count: 0,
                realCount: 0
            }))
    }

    public countGroupAnswers (): void {
        this.groups = this.groups.map(group => {
            group.count = group.answers
                .join('')
                .split('')
                .unique().length

            return group
        })
    }

    public countCoincidentAnswers (): void {
        this.groups[this.groups.length - 1].answers.pop()

        this.groups = this.groups.map(group => {
            let realCount = 0,
                firstAnswer = group.answers[0].split('')

            firstAnswer.forEach(question => {
                if (group.answers.slice(1).every(item => item.includes(question))) {
                    realCount++
                }
            })

            group.realCount = realCount

            return group
        })
    }

    public get sumOfAnswers (): number {
        return this.groups.reduce((acc, val) => {
            return acc + val.count
        }, 0)
    }

    public get realSum (): number {
        return this.groups.reduce((acc, val) => {
            return acc + val.realCount
        }, 0)
    }
}
