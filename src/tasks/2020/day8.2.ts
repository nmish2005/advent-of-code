import DataProvider from "../../helpers/DataProvider";
import { sessionId, year } from "../../env";
import GameConsole from "../../helpers/GameConsole";

async function run () {
    const provider = new DataProvider({
        taskId: 8,
        year,
        sessionId
    })
    let rawData = (await provider.getData())
    // const rawData = 'nop +0\n' +
    //     'acc +1\n' +
    //     'jmp +4\n' +
    //     'acc +3\n' +
    //     'jmp -3\n' +
    //     'acc -99\n' +
    //     'acc +1\n' +
    //     'jmp -4\n' +
    //     'acc +6'
    const Console = new GameConsole(rawData)

    Console.fixProgram()

    return Console.accumulatedValue
}

run()
    .then(console.log)
